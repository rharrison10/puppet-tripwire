# Install and configure Open Source Tripwire®
#
# https://github.com/Tripwire/tripwire-open-source/
#
# @summary Main class for installing tripwire.
#
# @param ensure
#   Should tripwire be `present` or `absent`
#
# @param tw_site_pass
#   The site passphrase used by tripwire
#
# @param tw_local_pass
#   The local passphrase used by tripwire
#
# @param config_file
#   The location of the config file on the filesystem.
#
# @param crit_dirs
#   The directories that `gen_twpol.py` will scan to build the policy.
#
# @param globalemail
#   List of email addresses separated by either a comma `,`, or semi-colon `;`.
#   If a report would have normally been sent out, it will also be sent to this
#   list of recipients.
#
# @param localkeyfile
#   The location of the local key file on the filesystem.
#
# @param mailnoviolations
#   This option controls the way that Tripwire sends email notification if no
#   rule violations are found during an integrity check.  If `mailnoviolations`
#   is set to false and no violations are found, Tripwire will not send a
#   report. With any other value, or if the variable is removed from the
#   configuration file, Tripwire will send an email message stating that no
#   violations were found.
#
# @param package_manage
#   Should the module manage the `tripwire` package.
#
# @param package_name
#   The name of the package to be managed.
#
# @param polfile
#   The location of the compiled policy on the filesystem.
#
# @param skip_dirs
#   The directories that `gen_twpol.py` will skip over when scanning to build
#   the policy.
#
# @param sitekeyfile
#   The location of the site key file on the filesystem.
#
# @param txt_cfg
#   The loation of the plain text config file on the filesystem.
#
# @param txt_pol
#   The location of the plain text policy on the filesystem.
#
# @param txt_pol_header
#   The location of the plain text header `gen_twpol.py` will use when building
#   the policy.
#
# @param varlog_monitor
#   Should tripwire monitor the contents of `/var/log` for changes? Note this
#   will cause tripwire to report violations every time [logrotate](https://github.com/logrotate/logrotate)
#   runs since there will be new files created, files removed, and many files
#   will change their inode. You'll need to update the tripwire database each
#   time this happens. If you don't want to be notified of such changes set this
#   to `false`.
#
# @param varlog_prop_mask
#   The property mask for monitoring log files. This can be any valid value as
#   defined by the "Property Masks" section of `man twpolicy` or a variable
#   defined in `twpol_header.txt.epp`. Defaults to: `'$(SEC_CONFIG)'`
#
# @param varlog_severity
#   Severity level for the `/var/log` rule. Either one of the variables defined
#   in `twpol_header.txt.epp` or an integer between `0` and `1000000`. Defaults
#   to: `'$(SIG_HI)'`
#
# @example Basic configuration with the defaults.
#   class { 'tripwire':
#     tw_local_pass => 'SomeLocalPass',
#     tw_site_pass  => 'SomeSecretSitePass',
#   }
#
class tripwire (
  String                    $tw_local_pass,
  String                    $tw_site_pass,
  Enum['absent', 'present'] $ensure           = present,
  String                    $config_file      = $::tripwire::params::config_file,
  Array                     $crit_dirs        = $::tripwire::params::crit_dirs,
  Optional[String]          $globalemail      = undef,
  String                    $localkeyfile     = $::tripwire::params::localkeyfile,
  Enum['false', 'true']     $mailnoviolations = $::tripwire::params::mailnoviolations, # lint:ignore:quoted_booleans
  Boolean                   $package_manage   = true,
  String                    $package_name     = $::tripwire::params::package_name,
  String                    $polfile          = $::tripwire::params::polfile,
  String                    $sitekeyfile      = $::tripwire::params::sitekeyfile,
  Array                     $skip_dirs        = $::tripwire::params::skip_dirs,
  String                    $txt_cfg          = $::tripwire::params::txt_cfg,
  String                    $txt_pol          = $::tripwire::params::txt_pol,
  String                    $txt_pol_header   = $::tripwire::params::txt_pol_header,
  Boolean                   $varlog_monitor   = true,
  String                    $varlog_prop_mask = $::tripwire::params::varlog_prop_mask,
  Variant[Enum['$(SIG_HI)', '$(SIG_MED)', '$(SIG_LOW)'], Integer[0, 1000000]]
                            $varlog_severity  = $::tripwire::params::varlog_severity,
) inherits tripwire::params {

  if $package_manage {
    contain tripwire::package
  }

  if $ensure == 'present' {
    contain tripwire::config
    Class['Tripwire::Package'] -> Class['Tripwire::Config']
  }

}
