# Default settings for tripwire module variables
#
# @summary Defaults for the tripwire module
#
# @api private
#
class tripwire::params {

  $config_file      = '/etc/tripwire/tw.cfg'
  $crit_dirs        = [
    '/boot',
    '/sbin',
    '/bin',
    '/usr/sbin',
    '/usr/bin',
    '/lib',
    '/usr/lib',
    '/usr/lib32',
    '/usr/lib64',
    '/usr/libexec',
    '/etc',
  ]
  $localkeyfile     = "/etc/tripwire/${::fqdn}-local.key"
  $mailnoviolations = 'true' # lint:ignore:quoted_booleans
  $package_name     = 'tripwire'
  $polfile          = '/etc/tripwire/tw.pol'
  $sitekeyfile      = '/etc/tripwire/site.key'
  $skip_dirs        = ['/etc/tripwire']
  $txt_cfg          = '/etc/tripwire/twcfg.txt'
  $txt_pol          = '/etc/tripwire/twpol.txt'
  $txt_pol_header   = '/etc/tripwire/twpol_header.txt'
  $varlog_prop_mask = '$(SEC_CONFIG)'
  $varlog_severity  = '$(SIG_HI)'

}
