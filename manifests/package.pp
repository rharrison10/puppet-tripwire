# Manage the tripwire pacakge
#
# @summary Manage the tripwire package
#
# @api private
#
# @param ensure Should tripwire be `present` or `absent`
#
# @param package_name The name of the package to be managed.
#
class tripwire::package (
  Enum['absent', 'present'] $ensure       = $::tripwire::ensure,
  String                    $package_name = $::tripwire::package_name,
) inherits tripwire {

  package { $package_name:
    ensure => $ensure,
  }
}
