require 'spec_helper'

describe 'tripwire::config' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:pre_condition) { "class { 'tripwire': tw_local_pass => 'SomeLocalPass', tw_site_pass  => 'SomeSecretSitePass',}" }

      it { is_expected.to compile }
    end
  end
end
